import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk"
import productsReducer from "./store/reducers/productsReducer"
import cartReducer from "./store/reducers/cartReducer"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const rootReducer = combineReducers({
    products: productsReducer,
    cart: cartReducer,
})

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));


const app = (
    <Provider store={store}>
            <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
