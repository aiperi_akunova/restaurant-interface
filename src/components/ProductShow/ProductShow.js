import React from 'react';
import './ProductShow.css';
import {useDispatch} from "react-redux";
import {addToCart, createCart} from "../../store/actions/cartActions";

const ProductShow = props => {

    return (
        <div className='product-box'>
            <div>
                <img src={props.image} alt="product"/>
            </div>
            <div>
                <h3>{props.name}</h3>
                <p><b>KGS:</b> {props.price}</p>
            </div>
            <div>
                <button className='add-cart-btn' onClick={props.onAdd}>Add to cart</button>
            </div>
        </div>
    );
};

export default ProductShow;