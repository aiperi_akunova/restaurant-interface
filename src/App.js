import './App.css';
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect} from "react";
import {requestProducts} from "./store/actions/productsActions";
import ProductShow from "./components/ProductShow/ProductShow";
import {createCart} from "./store/actions/cartActions";

const App = () => {

    const dispatch = useDispatch();
    const productsState = useSelector(state => state.products.products);
    const cartState = useSelector(state => state.cart.orders);

    useEffect(()=>{
        const fetchData = async ()=>{
          await  dispatch(requestProducts());
        }
       fetchData();
    }, [dispatch]);

    const onAdd=(obj,cart)=>{
        dispatch(createCart(obj, cart));
    }

  return (
      <div className="App">
          <h1>Welcome to our online shop</h1>
          <div className='container'>
              <div className='products'>
                  {productsState && productsState.map(prod=>(
                      <ProductShow
                          name = {prod.name}
                          price = {prod.price}
                          image = {prod.image}
                          key = {prod.name}
                         onAdd={()=>onAdd(prod,cartState)}
                      />
                  ))}
              </div>
              <div className='cart'>
                  <h3>Your cart!</h3>

                  {cartState && cartState.map(item=>(
                      <p>{item.name} x {item.amount}</p>
                  ))}
              </div>
          </div>
      </div>
  );
};

export default App;
