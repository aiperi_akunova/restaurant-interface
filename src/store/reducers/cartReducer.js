import {ADD_TO_CART, COUNT_TOTAL_CART, REMOVE_IN_CART} from "../actions/cartActions";

const initialState = {
    orders: []
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            return {...state, orders: action.payload}
        default:
            return state;
    }
};

export default cartReducer;