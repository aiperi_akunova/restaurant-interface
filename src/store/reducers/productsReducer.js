import {PRODUCTS_FAILURE, PRODUCTS_REQUEST, PRODUCTS_SUCCESS} from "../actions/productsActions";

const initialState = {
    products: null,
    loading: false,
    error: null,
};

const productsReducer = (state = initialState, action)=>{

    switch (action.type){
        case PRODUCTS_REQUEST:
            return {...state, error: null, loading: true};
        case PRODUCTS_SUCCESS:
            return {...state, loading: false, products: action.payload};
        case PRODUCTS_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }

}

export default productsReducer;
