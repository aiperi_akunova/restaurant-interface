import axios from "axios";

export const PRODUCTS_REQUEST = 'PRODUCTS_REQUEST';
export const PRODUCTS_SUCCESS = 'PRODUCTS_SUCCESS';
export const PRODUCTS_FAILURE = 'PRODUCTS_FAILURE';

export const productsRequest = ()=>({type:PRODUCTS_REQUEST})
export const productsSuccess = (response)=>({type:PRODUCTS_SUCCESS, payload: response})
export const productsFailure = error=>({type:PRODUCTS_FAILURE, payload:error});

export const requestProducts = () =>{
    return async dispatch =>{
        try{
            dispatch(productsRequest());
            const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/dishes.json');
            dispatch(productsSuccess(response.data));
        } catch (error){
            dispatch(productsFailure(error))
        }
    }
}