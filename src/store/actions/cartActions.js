export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_IN_CART = 'REMOVE_IN_CART';
export const COUNT_TOTAL_CART = 'COUNT_TOTAL_CART';

export const addToCart = (data)=>({type: ADD_TO_CART, payload: data});
export const removeInCart = (data)=>({type: ADD_TO_CART, payload: data});
export const countTotalCart = ()=>({type: COUNT_TOTAL_CART});


export const createCart = (productObj, orderArray) =>{

    return dispatch => {
       let newOrderArray = [...orderArray];
        const exist = newOrderArray.findIndex(item=>item.name === productObj.name);

        if(exist === -1){
            newOrderArray=[...orderArray, {name: productObj.name, price: productObj.price, amount: 1}];
        } else {
            newOrderArray = newOrderArray.map(order=>{
             if(order.name === productObj.name){
                 return { ... order, amount: order.amount+1}
             }
            })
        }

        dispatch(addToCart(newOrderArray))
    }
}